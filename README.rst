#####################################
Toyota Freewheeling Hub Handle Wrench
#####################################


Summary
=======

This repo contains design files for a device to assist in the turning of
stubborn Toyota free-wheeling hub handles.  It’s perfect for people with
big old meathooks that can’t fit the recessed hub handles, people with
arthritic fingers, or those that just have flimsy wrists.

.. figure:: doc/images/hub_wrench.png
   :figwidth: 50%
   :width: 100%
   :alt: CAD rendering of hub wrench.

   CAD rendering of hub wrench.

The wrench is designed with FreeCAD_, a free (LGPL2+) parametric, 3-D
CAD software.  STL files generated from the design files can be found in
the ``/build`` directory.

.. figure:: doc/images/hub_wrencn_in_use.jpeg
   :figwidth: 50%
   :width: 100%
   :alt: Photo of hub wrench in use.

   Hub wrench in use.


.. _FreeCAD: https://www.freecad.org/



Future Plans
============

Clean up the 2-D/detail drawing.  It lacks polish and does not adhere to
best practices.



Copying
=======

Everything here is copyright © 2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
