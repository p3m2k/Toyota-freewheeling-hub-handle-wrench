# Project-Specific Configuration
#

# Meta
export NAME = $(shell $(call get_subst_var,SLUG))
export VERSION = $(shell $(call get_subst_var,RELEASE_VERSION))
export GIT_BRANCH = $(shell git branch --show-current)
export GIT_HEAD=$(shell git log -1 --pretty=format:%h)
