# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="Toyota-freewheeling-hub-handle-wrench"
SLUG="toyota-freewheeling-hub-handle-wrench"
DESCRIPTION="A device to assist in the turning of stubborn hub handles"
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2023"
RELEASE_VERSION="1.0.0"
RELEASE_DATE="2023-02-16"
